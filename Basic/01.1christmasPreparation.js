function christmasPreparation (input) {
    let opakovachnaHartiqPrice = 5.80;
    let platPrice = 7.20;
    let lepiloPrice = 1.20;

    let broiRolkiOpakovachnaHartiq = Number(input[0]);
    let broiRolkiPlat = Number(input[1]);
    let litriLepilo = Number(input[2]);
    let discountPercentage = Number(input[3]);

    let totalOpakovachnaHartiqPrice = broiRolkiOpakovachnaHartiq * opakovachnaHartiqPrice;
    let totalPlatPrice = broiRolkiPlat * platPrice;
    let totalLepiloPrice = litriLepilo * lepiloPrice;

    let totalPrice = totalOpakovachnaHartiqPrice + totalPlatPrice + totalLepiloPrice;

    let discount = (discountPercentage / 100) * totalPrice;
    let finalPrice = totalPrice - discount;

    console.log(finalPrice.toFixed(3));
}

christmasPreparation (["2", "3", "2.5", "25"])