function excursionCalculator(input) {
    let people = Number(input[0]);
    let season = input[1];
    let price = 0;

    if (season === 'spring') {
        if (people <= 5) {
            price = people * 50;
        } else {
            price = people * 48;
        }
    } else if (season === 'summer') {
        if (people <= 5) {
            price = people * 48.50;
        } else {
            price = people * 45;
        }
    } else if (season === 'autumn') {
        if (people <= 5) {
            price = people * 60;
        } else {
            price = people * 49.50;
        }
    } else if (season === 'winter') {
        if (people <= 5) {
            price = people * 86;
        } else {
            price = people * 85;
        }
    }

    if (season === 'summer') {
        price *= 0.85; 
    } else if (season === 'winter') {
        price *= 1.08; 
    }

    console.log(`${price.toFixed(2)} leva.`);
}

excursionCalculator(["10", "summer"])