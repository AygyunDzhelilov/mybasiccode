function maidenParty(input) {
    let loveMessagePrice = 0.60;
    let waxRosePrice = 7.20;
    let keychainPrice = 3.60;
    let cartoonPrice = 18.20;
    let luckySurprisePrice = 22.00;

    let bachelorettePartyPrice = Number(input[0]);
    let loveMessages = Number(input[1]);
    let waxRoses = Number(input[2]);
    let keychains = Number(input[3]);
    let cartoons = Number(input[4]);
    let luckySurprises = Number(input[5]);

    let totalLoveMessagesPrice = loveMessages * loveMessagePrice;
    let totalWaxRosesPrice = waxRoses * waxRosePrice;
    let totalKeychainsPrice = keychains * keychainPrice;
    let totalCartoonsPrice = cartoons * cartoonPrice;
    let totalLuckySurprisesPrice = luckySurprises * luckySurprisePrice;

    let totalItems = loveMessages + waxRoses + keychains + cartoons + luckySurprises;
    let totalPrice = totalLoveMessagesPrice + totalWaxRosesPrice + totalKeychainsPrice + totalCartoonsPrice + totalLuckySurprisesPrice;

    if (totalItems >= 25) {
        totalPrice *= 0.65; 
    }

    let hostingCost = totalPrice * 0.10;
    let finalAmount = totalPrice - hostingCost;

    if (finalAmount >= bachelorettePartyPrice) {
        let leftMoney = finalAmount - bachelorettePartyPrice;
        console.log(`Yes! ${leftMoney.toFixed(2)} lv left.`);
    } else {
        let neededMoney = bachelorettePartyPrice - finalAmount;
        console.log(`Not enough money! ${neededMoney.toFixed(2)} lv needed.`);
    }

}

maidenParty (["40.8", "20", "25", "30", "50", "10"])