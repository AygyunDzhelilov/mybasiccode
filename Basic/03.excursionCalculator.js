function excursionCalculator(input) {
    let people = Number(input[0]);
    let season = input[1];
    let price = 0;

    switch (season) {
        case 'spring':
            price = people <= 5 ? people * 50 : people * 48;
            break;

        case 'summer':
            price = people <= 5 ? people * 48.50 : people * 45;
            break;

        case 'autumn':
            price = people <= 5 ? people * 60 : people * 49.50;
            break;

        case 'winter':
            price = people <= 5 ? people * 86 : people * 85;
            break;

    }

    if (season === 'summer') {
        price *= 0.85; 
    } else if (season === 'winter') {
        price *= 1.08;
    }

    console.log(`${price.toFixed(2)} leva.`);
}

excursionCalculator(["10", "summer"])

