function christmasPreparation (input) {
    let hartiq = 5.80
    let plat = 7.20
    let lepilo = 1.20

    let broiRolkiOpakovachnaHartiq = Number(input[0]);
    let broiRolkiPlat = Number(input[1]);
    let litriLepilo = Number(input[2]);
    let procentNamalenie = Number(input[3]);

    let priceRolkiOpakovachnaHartiq = (broiRolkiOpakovachnaHartiq * hartiq);
    let priceRolkiPlat = (broiRolkiPlat * plat);
    let priceLepilo = (litriLepilo * lepilo);

    let allMateriali = (priceRolkiOpakovachnaHartiq + priceRolkiPlat + priceLepilo);

    let discount = (procentNamalenie / 100) * allMateriali;
    let finalPrice = allMateriali - discount;

    console.log (finalPrice.toFixed(3));
}

christmasPreparation (["4",
    "2",
    "5",
    "13"])